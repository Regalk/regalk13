# Hey there, I'm <a  href="https://github.com/regalk13/">Juan Salazar</a>

<img src="https://komarev.com/ghpvc/?username=regalk13&style=plastic" />

Current Software Engineer. 5+ years experience specializing in the backend development, infrastructure automation, and computer hacking/security. Super nerd who loves Vim and Linux, and enjoys to customize all of the development environment. Interested in devising a better problem-solving method for challenging tasks, and learning new technologies and tools if the need arises.

- 🌱 I’m currently focusing on **OpenGl C++** and practicing **Data Structures and Algorithms** everyday.
- 💬 I’m looking forward to contribute to **Open Source Projects**.
- 🐍 I will write some tech post [here](https://dev.to/regalk13).
- ⚡ Fun fact: I use arch btw. 
- 🔌 Reach me on [twitter](https://twitter.com/regalk4)


## Languages and Tools
### FrontEnd:
<p align="left"> 
<img src="https://img.icons8.com/color/48/000000/html-5--v1.png"/><img src="https://img.icons8.com/color/48/000000/css3.png"/><img src="https://img.icons8.com/color/48/000000/javascript--v1.png"/><img src="https://img.icons8.com/color/48/000000/sass-avatar.png"/></p>

### BackEnd:
 <p align="left">
<img src="https://img.icons8.com/color/48/4a90e2/c-programming.png"/><img src="https://img.icons8.com/color/48/4a90e2/c-plus-plus-logo.png"/><img src="https://cdn.cdnlogo.com/logos/c/27/c.svg" width=43><img src="https://img.icons8.com/color/48/4a90e2/python--v1.png"/><img src="https://img.icons8.com/color/48/4a90e2/java-coffee-cup-logo--v1.png"/><img src="https://img.icons8.com/fluency/48/000000/node-js.png"width=43/><img src="https://img.icons8.com/color/48/000000/django.png"/></p>

### Db's & Tools:
<p align="left">
  <img src="https://www.vectorlogo.zone/logos/neovimio/neovimio-icon.svg" width=43/><img src="https://img.icons8.com/color/48/000000/amazon-web-services.png"/><img src="https://img.icons8.com/color/48/000000/sql.png"/><img src="https://img.icons8.com/color/48/000000/maria-db.png"/><img src="https://img.icons8.com/color/50/000000/postgreesql.png"/><img src="https://img.icons8.com/color/48/000000/mongodb.png"/><img src="https://img.icons8.com/color/48/4a90e2/git.png"/><img src="https://img.icons8.com/fluent/48/4a90e2/github.png"/><img src="https://img.icons8.com/color/48/000000/linux--v1.png"/></p>

## Stats
<img src = "https://github-readme-stats.vercel.app/api?username=regalk13&show_icons=true&theme=dark" width = 500 style="display: flex">

